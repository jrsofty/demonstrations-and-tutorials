package org.bitbucket.jrsofty.blogpostdemo.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("/blog")
public interface BlogPostREST {

    @GET
    @Path("/test")
    @Produces("text/plain;charset=utf-8")
    public Response test();

}
