package org.bitbucket.jrsofty.blogpostdemo;

import org.bitbucket.jrsofty.blogpostdemo.rest.BlogPostRESTImpl;
import org.glassfish.jersey.server.ResourceConfig;

public class ApplicationLaunch extends ResourceConfig {

    public ApplicationLaunch() {
        this.register(BlogPostRESTImpl.class);
    }

}
