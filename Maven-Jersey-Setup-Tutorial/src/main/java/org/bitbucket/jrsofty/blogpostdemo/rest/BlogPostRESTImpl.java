package org.bitbucket.jrsofty.blogpostdemo.rest;

import javax.ws.rs.core.Response;

public class BlogPostRESTImpl implements BlogPostREST {

    @Override
    public Response test() {
        return Response.ok("Yep it's working").build();
    }

}
